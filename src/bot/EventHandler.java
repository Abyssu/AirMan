package bot;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import commands.Command;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.ReadyEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IGuild;

public class EventHandler
{
	public static List<Command> commands;

	protected void loadCommands()
	{
		commands = new ArrayList<Command>();
		File folder = new File("bin/commands");
		File[] classFiles = folder.listFiles();

		try
		{
			for (File file : classFiles)
			{
				// Ignore all three abstract classes
				if (!file.getName().equals("Command.class"))
				{
					String className = file.getName().split("\\.")[0];
					String classPath = "commands." + className;
					Class<?> cls = Class.forName(classPath);
					Command cmd = Command.class.cast(cls.newInstance());
					commands.add(cmd);
					System.out.println(className);
				}
			}
		}
		catch (ClassNotFoundException | InstantiationException | IllegalAccessException e)
		{
			System.err.print("Classes couldn't been instantiated via reflection.\n");
			e.printStackTrace();
		}
	}

	@EventSubscriber
	public void handle(ReadyEvent readyEvent)
	{
		try
		{
			// Store bot and owner users
			AirMan.bot = AirMan.client.getOurUser();

			// Find owner
			long ownerid = Long.parseLong(AirMan.ini.get("Permissions", "OwnerID")); // Load
																						// owner
																						// id
			AirMan.owner = AirMan.client.getUserByID(ownerid);

			// Minor information upon connecting
			System.out.println("Connected!\n");
			System.out.printf("Bot:\t%s\\%s\n", AirMan.bot.getLongID(), AirMan.bot.getName());
			System.out.printf("Owner:\t%s\\%s\n\n", AirMan.owner.getLongID(), AirMan.owner.getName());

			// List of every guild the bot is connected to
			System.out.println("Server List:");
			List<IGuild> guilds = AirMan.client.getGuilds();
			for (IGuild guild : guilds)
			{
				System.out.printf(" - %s\n", guild.getName());
			}

			// Prefix the bot will respond to
			System.out.printf("\n\n Command Prefix: %s\n", AirMan.botPrefix);

			loadCommands(); // Dynamically loads all subclasses in the commands
							// package

		}
		catch (NullPointerException e)
		{
			System.err.println("Owner not found... retrying...");
			handle(readyEvent);
		}

	}

	@EventSubscriber
	public void onMessageReceived(MessageReceivedEvent event)
	{
		String text = event.getMessage().getContent(); // Retrieve text from
														// message

		// Check if a command was actually called
		if (event.getMessage().getContent().startsWith(AirMan.botPrefix))
		{
			String[] arguments = text.split(" ");
			String command = arguments[0].substring(1); // Remove prefix

			// Using lambdas to find a matching command
			Command match = commands.stream().filter(c -> c.matchesAlias(command)).findFirst().orElse(null);

			if (match != null) // Command match
			{
				String[] args;
				try
				{
					// Remove command call from arguments
					args = Arrays.copyOfRange(arguments, 1, arguments.length);
					match.run(event, args); // Run the command
				}
				catch (IndexOutOfBoundsException e) // Can't instantiate an
													// empty array
				{
					args = null;
					match.run(event, null);
				}
			}

			// Reaction images
			else
			{
				String image = text.substring(1); // Remove prefix

				File folder = new File("images");
				File[] imageFolders = folder.listFiles();
				File matchingFolder = null;
				
				// Find folder
				for (File f : imageFolders)
				{
					if (image.startsWith(f.getName().toLowerCase()))
						matchingFolder = f;
				}

				// Reaction found
				if (matchingFolder != null) 
				{
					File[] images = matchingFolder.listFiles();
					String message = event.getAuthor().mention();

					AirMan.sendFile(event.getChannel(), message, images[(int) (Math.random() * images.length)]);
				}
			}
		}

	}
}
