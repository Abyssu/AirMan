package bot;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;

import org.ini4j.Ini;

import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.handle.obj.IChannel;
import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.handle.obj.IUser;
import sx.blah.discord.util.DiscordException;
import sx.blah.discord.util.RequestBuffer;

public class AirMan
{
	public static IDiscordClient client;
	public static String botPrefix;
	public static IUser bot;
	public static IUser owner;
	public static Ini ini;
	public static long modRoles [];
	public static boolean spam;

	// TODO: figure out how I should handle permissions
	public static void main(String[] args)
	{
		try
		{
			// Loading credentials from ini file
			ini = new Ini(new File("config.ini"));
			botPrefix = ini.get("Chat", "CommandPrefix"); // Load command prefix
			String modRoleIds = ini.get("Permissions", "ModRoles");
			
			if (!modRoleIds.isEmpty())
			{
				modRoles = Arrays.stream(modRoleIds.split(" ")).mapToLong(i -> Long.parseLong(i)).toArray();
			}

			client = new ClientBuilder().withToken(ini.get("Credentials", "Token")).build(); // Prepare client to log in with loaded token
			client.getDispatcher().registerListener(new EventHandler()); // Start event handler
			client.login(); // Login
		}
		catch (Exception e) // Most likely missing ini file or wrong name
		{
			e.printStackTrace();
			System.err.println("There was a problem with the config.ini file...");
		}

	}

	public static void sendMessage(IChannel channel, String message)
	{
		// The buffer is an attempt to not hit the rate limit
		RequestBuffer.request(() -> {
			try
			{
				channel.sendMessage(message);
			}
			catch (DiscordException e)
			{
				System.err.println("Message could not be sent with error: ");
				e.printStackTrace();
			}
		});
	}
	
	public static void deleteMessage(IChannel channel, IMessage message)
	{
		// The buffer is an attempt to not hit the rate limit
		RequestBuffer.request(() -> {
			try
			{
				message.delete();
			}
			catch (DiscordException e)
			{
				System.err.println("Message could not be sent with error: ");
				e.printStackTrace();
			}
		});
	}
	
	public static void sendFile(IChannel channel, String message, File file)
	{
		RequestBuffer.request(() -> {
			try
			{
				channel.sendFile(message, file);
			}
			catch (DiscordException | FileNotFoundException e)
			{
				System.err.println("Message could not be sent with error: ");
				e.printStackTrace();
			}
		});
	}
}
