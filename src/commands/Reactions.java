package commands;

import java.io.File;

import bot.AirMan;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

public class Reactions extends Command
{

	public Reactions()
	{
		StringBuilder desc = new StringBuilder("Usage:\t");
		desc.append(AirMan.botPrefix);
		desc.append("reactions\n");
		desc.append("Prints a list of reactions.\n");

		description = desc.toString();
		aliases = new String[] { "reactions" };
		permissionLevel = 0;
	}

	@Override
	public void permitted(MessageReceivedEvent event, String[] args)
	{
		File folder = new File("images");
		File[] imageFolders = folder.listFiles();
		String [] reactionNames = new String[imageFolders.length];
		
		for (int x = 0; x < imageFolders.length; x++)
		{
			reactionNames[x] = imageFolders[x].getName();
		}
		
		StringBuilder reactionList = new StringBuilder("***Reactions***\n```\n");
		reactionList.append(String.join(", ", reactionNames));
		reactionList.append("\n```");
		AirMan.sendMessage(event.getChannel(), reactionList.toString());
	}

}
