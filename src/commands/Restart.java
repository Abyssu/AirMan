package commands;

import bot.AirMan;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

public class Restart extends Command
{

	public Restart()
	{
		StringBuilder desc = new StringBuilder("Usage:\t");
		desc.append(AirMan.botPrefix);
		desc.append("restart (pull)\n");
		desc.append("Restarts the bot.\n");
		desc.append(
				"If argument \"pull\" is specified, the bot will pull the latest build. (Owner permission level)\n");

		description = desc.toString();
		aliases = new String[] { "restart" };
		permissionLevel = 1;
	}

	@Override
	public void permitted(MessageReceivedEvent event, String[] args)
	{
		if (args == null)
		{
			AirMan.client.logout();
			System.exit(0);
		}

		else if (args[0].equalsIgnoreCase("pull"))
		{
			int userLevel = getUserPermissionLevel(event);
			if (userLevel == 2)
			{
				AirMan.client.logout();
				System.exit(1);
			}
			else
			{
				String[] levelNames = { "Pleb", "Mod", "Owner" };
				String denied = String.format(
						"```\nYou do not have permission to use this command. (Access level: %s)\n```",
						levelNames[userLevel]);

				AirMan.sendMessage(event.getChannel(), denied);
			}
		}
	}

}
