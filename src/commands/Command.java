package commands;

import java.util.Arrays;
import java.util.List;

import bot.AirMan;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.handle.obj.IRole;
import sx.blah.discord.handle.obj.IUser;

public abstract class Command
{
	protected String description; // Holds command description
	protected String aliases[]; // Holds command aliases
	protected int permissionLevel; // Holds permission level (0 = anyone, 1 =
									// mod, 2 = owner)

	// Generic constructor
	public Command()
	{

	}

	// Accessor for aliases
	public String[] getAliases()
	{
		return aliases;
	}

	// Accessor for description
	public String getDescription()
	{
		return description;
	}

	public int getPermissionLevel()
	{
		return permissionLevel;
	}

	// Access to commands based off of permission level, subclasses default to
	// normal permission level
	public boolean getPermitted(int userLevel)
	{
		return userLevel >= permissionLevel;
	}

	public int getUserPermissionLevel(MessageReceivedEvent event)
	{
		IUser author = event.getAuthor();

		List<IRole> roles = author.getRolesForGuild(event.getGuild());


		int userLevel = 0;
		
		// I'm actually mad I couldn't get any good ideas working
		for (IRole role : roles)
		{
			
			for (long id : AirMan.modRoles)
			{
				if (id == role.getLongID())
				{
					userLevel = 1;
					break;
				}
			}
			if (userLevel == 1)
				break;
		}

		if (author == AirMan.owner)
			userLevel = 2;
		
		return userLevel;
	}

	public void run(MessageReceivedEvent event, String[] args)
	{
		int userLevel = getUserPermissionLevel(event);

		String[] levelNames = { "Pleb", "Mod", "Owner" };

		if (getPermitted(userLevel))
		{
			permitted(event, args);
		}

		else
		{
			String denied = String.format(
					"```\nYou do not have permission to use this command. (Access level: %s)\n```",
					levelNames[userLevel]);

			AirMan.sendMessage(event.getChannel(), denied);
		}

	}

	// Returns true if any alias for the command matches a command call
	public boolean matchesAlias(String command)
	{
		return Arrays.stream(aliases).anyMatch(s -> command.equalsIgnoreCase(s));
	}

	// Executes the command
	public abstract void permitted(MessageReceivedEvent event, String[] args);
}
