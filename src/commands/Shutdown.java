package commands;

import bot.AirMan;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

public class Shutdown extends Command
{

	public Shutdown()
	{
		StringBuilder desc = new StringBuilder("Usage:\t");
		desc.append(AirMan.botPrefix);
		desc.append("shutdown\n");
		desc.append("Kills the bot.\n");
		
		description = desc.toString();
		aliases = new String[]{"shutdown", "die"};
		permissionLevel = 2;
	}
	
	@Override
	public void permitted(MessageReceivedEvent event, String[] args)
	{
		AirMan.client.logout();
		System.exit(2);
	}


}
