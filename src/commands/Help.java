package commands;

import java.util.ArrayList;
import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;

import bot.AirMan;
import bot.EventHandler;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;

public class Help extends Command
{

	public Help()
	{
		StringBuilder desc = new StringBuilder("Usage:\t");
		desc.append(AirMan.botPrefix);
		desc.append("help [command]\n");
		desc.append("Prints a help message.\n");
		desc.append("If a command is specified, it prints a help message for that command.\n");
		desc.append("Otherwise, it lists the available commands.");

		description = desc.toString();
		aliases = new String[] { "help" };
		permissionLevel = 0;
	}

	@Override
	public void permitted(MessageReceivedEvent event, String[] args)
	{
		// List all commands as no arguments were entered
		if (args == null)
		{
			// Prepare to build strings for all permission levels
			StringBuilder message = new StringBuilder("**Commands**\n```");
			StringBuilder modMessage = new StringBuilder("**High Permission Commands**\n```");
			StringBuilder ownerMessage = new StringBuilder("**Owner Commands**\n```");

			// Retrieve all commands and their aliases
			for (Command command : EventHandler.commands)
			{
				ArrayList<String> commandAliases = new ArrayList<>(Arrays.asList(command.getAliases()));

				switch (command.getPermissionLevel())
				{

					// Default permission level
					case 0:
					{
						// No aliases
						if (commandAliases.size() == 1)
							message.append("\n").append(commandAliases.get(0));

						// At least 1 alias
						else if (commandAliases.size() > 1)
						{
							message.append("\n").append(commandAliases.get(0));
							commandAliases.remove(0);
							message.append(" (alias(es): ").append(String.join(", ", commandAliases)).append(")");
						}

						break;
					}

					// Mod permission level
					case 1:
					{
						// No aliases
						if (commandAliases.size() == 1)
							modMessage.append("\n").append(commandAliases.get(0));

						// At least 1 alias
						else if (commandAliases.size() > 1)
						{
							modMessage.append("\n").append(commandAliases.get(0));
							commandAliases.remove(0);
							modMessage.append(" (alias(es): ").append(String.join(", ", commandAliases)).append(")");
						}

						break;
					}

					// Owner permission level
					case 2:
					{
						// No aliases
						if (commandAliases.size() == 1)
							ownerMessage.append("\n").append(commandAliases.get(0));

						// At least 1 alias
						else if (commandAliases.size() > 1)
						{
							ownerMessage.append("\n").append(commandAliases.get(0));
							commandAliases.remove(0);
							ownerMessage.append(" (alias(es): ").append(String.join(", ", commandAliases)).append(")");
						}

						break;
					}

				}
			}

			// End text block
			message.append("\n```");
			modMessage.append("\n```");
			ownerMessage.append("\n```");

			// Merge all blocks the user has access to
			if (StringUtils.countMatches(modMessage.toString(), "\n") > 2)
				message.append("\n\n").append(modMessage.toString());

			if (StringUtils.countMatches(ownerMessage.toString(), "\n") > 2)
				message.append("\n\n").append(ownerMessage.toString());

			AirMan.sendMessage(event.getChannel(), message.toString());
		}

		else
		{
			if (args[0].startsWith(AirMan.botPrefix))
				args[0] = args[0].substring(1);

			// Using lambdas to find a matching command
			Command match = EventHandler.commands.stream().filter(c -> c.matchesAlias(args[0])).findFirst()
					.orElse(null);

			if (match == null)
			{
				AirMan.sendMessage(event.getChannel(), String.format("```\n%s\n```", this.getDescription()));
			}

			else
			{
				AirMan.sendMessage(event.getChannel(), String.format("```\n%s\n```", match.getDescription()));
			}
		}

	}

}
